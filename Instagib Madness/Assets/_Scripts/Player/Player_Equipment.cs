﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.UI;

public class Player_Equipment : NetworkBehaviour
{
    #region Fields
    private int equippedWeaponIndex = -1;

    // This array contains all weapons the player could potentially aquire in the game.
    [SerializeField]
    private GameObject[] weapons;

    // The location at which the weapon is displayed.
    [SerializeField]
    private Transform weaponSlot;

    // The actual weapon the player has qeuipped.
    [SerializeField]
    private GameObject currentWeapon;

    // Used to display stats of equipped items.
    private Player_HUD HUD;

    // A list of weapons the player has at their disposal, but are not currently using.
    private List<GameObject> inactiveWeapons = new List<GameObject>();
    #endregion   

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        HUD = GetComponent<Player_HUD>();
    }

    /// <summary>
    /// This method is the hook function for the SyncVar 'equippedWeaponIndex'.
    /// It will be called every time the SyncVar changes on a client.
    /// </summary>
    /// <param name="weaponIndex"></param>
    [ClientRpc]
    private void RpcEquipWeapon(int weaponIndex)
    {
        // -1 means there should be no weapon equipped.
        if (weaponIndex == -1)
        {
            // Unequip the weapon;
            if (currentWeapon != null)
            {
                // 'Disable' the weapon for the local player.
                if (isLocalPlayer)
                {
                    Weapon weapon = (Weapon)currentWeapon.GetComponent(typeof(Weapon));
                    // Tell the weapon it has to be picked up again in order to function properly.
                    weapon.obtainedByLocalPlayer = false;

                    // Update the HUD.
                    HUD.SetCurrentWeapon(null);
                    HUD.UpdateHUD();
                }

                // 'Hide' the weapon on all clients
                currentWeapon.SetActive(false);
                currentWeapon = null;
            }

            // Don't equip any weapon (code below)
            return;
        }

        Debug.Log("Equipping weapon " + weaponIndex + " for " + gameObject.name);

        if (currentWeapon != null) currentWeapon.SetActive(false);

        currentWeapon = weapons[weaponIndex];

        currentWeapon.SetActive(true);

        // Make the weapon controllable by the local player only.
        if (isLocalPlayer)
        {
            // Tell the weapon that its owner is the local player.
            Weapon weaponScript = currentWeapon.GetComponent(typeof(Weapon)) as Weapon;
            weaponScript.ownedByLocalPlayer = true;

            // Set the weapons camTransform
            weaponScript.SetCamTransform(GetComponentInChildren<Camera>().transform);

            // Tell the HUD which weapon is equipped.
            HUD.SetCurrentWeapon(weaponScript);

            // Update the HUD.
            HUD.UpdateHUD();

            Debug.Log("Equipped weapon should be ready to fire");
        }
    }

    /// <summary>
    /// Refill the weapon's ammo supply by a certain amount and equip it.
    /// NOTE: The equipping part may be removed in the future.
    /// </summary>
    /// <param name="weaponIndex"></param>
    public void ObtainWeapon(int weaponIndex)
    {
        ((Weapon)weapons[weaponIndex].GetComponent(typeof(Weapon))).RestockAmmo(15);
        if (isLocalPlayer) HUD.UpdateHUD();

        if (weaponIndex != equippedWeaponIndex) EquipWeapon(weaponIndex);
    }

    public void EquipWeapon(int weaponIndex)
    {
        // ==========================================================================================
        // Hotfix: Wait for the weapon to complete its coroutines before disabling it. (Wait for cooldown and reload to end)
        // ==========================================================================================
        if ((currentWeapon != null && ((Weapon)currentWeapon.GetComponent(typeof(Weapon))).GetIsOnCooldown()) || (currentWeapon != null && ((Weapon)currentWeapon.GetComponent(typeof(Weapon))).GetIsReloading()))
        {
            Debug.Log("Hotfix: Wait for current weapon to complete cooldown before equipping another.");
            return;
        }

        equippedWeaponIndex = weaponIndex;

        CmdEquipWeapon(weaponIndex);
    }

    [Command]
    void CmdEquipWeapon(int weaponIndex)
    {
        RpcEquipWeapon(weaponIndex);
    }

    /// <returns> 999 if the player has no weapon equipped. Else equippedWeaponIndex.</returns>
    public int GetEquippedWeaponIndex()
    {
        if (currentWeapon != null) return equippedWeaponIndex;
        else return 999;
    }


}
