﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_Setup : NetworkBehaviour {

    [SerializeField]
    Camera FPSCharacterCam;
    [SerializeField]
    AudioListener audioListener;
    [SerializeField]
    GameObject HUD;
    [SerializeField]
    Player_Equipment equipment;

    private GameState gameState;
    private GameStartup gameStartup;

    public override void OnStartLocalPlayer()
    {
        gameState = GameObject.Find("GameManager").GetComponent<GameState>();
        gameStartup = GameObject.Find("GameManager").GetComponent<GameStartup>();

        // Notify the server that this player has entered the game.
        CmdRegisterToGameStartup(gameObject.name);        
    }

    void Update()
    {
        if (isLocalPlayer)
        {
            if (gameState.gameStarted)
            {
                EnablePlayer();
            }
        }        
    }

    private void EnablePlayer()
    {
        if (isLocalPlayer)
        {
            // Disable the pre-game cam.
            gameStartup.DisablePreGameCam();

            // Enable essential scripts on this player.
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
            GetComponent<Player_InputManager>().enabled = true;
            FPSCharacterCam.enabled = true;
            audioListener.enabled = true;

            HUD.SetActive(true);            
        }

        // Disable the script after the player has been enabled.
        this.enabled = false;
    }

    [Command]
    void CmdRegisterToGameStartup(string myID)
    {
        GameObject.Find("GameManager").GetComponent<GameStartup>().RegisterPlayer(myID);
    }
}
