﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_SoundManager : NetworkBehaviour
{

    [SyncVar(hook = "RpcPlayGunfireSound")]
    private Weapon.WeaponType weaponFired;

    [SerializeField]
    private AudioSource weaponSource;

    [SerializeField]
    private AudioSource playerSource;

    [SerializeField]
    private AudioClip death;

    [SerializeField]
    private AudioClip rifleGunfire;

    [SerializeField]
    private AudioClip rifleNoAmmo;

    [SerializeField]
    private AudioClip rifleReloadStart;

    [SerializeField]
    private AudioClip rifleReloadEnd;

    private bool muted = false;

    void Start()
    {
        // Do this for all players in the client scene.
        Player_InputManager.OnButtonPressed_Mute += Mute;
    }

    public override void OnNetworkDestroy()
    {
        base.OnNetworkDestroy();
        Player_InputManager.OnButtonPressed_Mute -= Mute;
    }

    // Mute
    private void Mute()
    {
        if (!muted)
        {
            muted = true;

            playerSource.Pause();
            playerSource.volume = 0;

            weaponSource.Pause();
            weaponSource.volume = 0;
        }
        else
        {
            muted = false;

            playerSource.UnPause();
            playerSource.volume = playerSource.maxDistance;

            weaponSource.UnPause();
            weaponSource.volume = weaponSource.maxDistance;
        }
    }


    #region Player related methods
    [ClientRpc]
    private void RpcPlayDeathSound()
    {
        playerSource.PlayOneShot(death);
    }

    [Command]
    void CmdPlayDeathSound()
    {
        RpcPlayDeathSound();
    }

    public void PlayDeathsound()
    {
        CmdPlayDeathSound();
    }

    #endregion

    #region Weapon related methods

    [ClientRpc]
    private void RpcPlayGunfireSound(Weapon.WeaponType weaponFired)
    {
        if (isLocalPlayer) return;

        switch (weaponFired)
        {
            case Weapon.WeaponType.Rifle:
                weaponSource.PlayOneShot(rifleGunfire);
                break;
        }
    }

    /// <summary>
    /// Play the sound locally before sending a the command to play it everywhere.
    /// </summary>
    /// <param name="weaponFired"></param>
    public void PlayGunfireSound(Weapon.WeaponType weaponFired)
    {
        if (isLocalPlayer)
        {
            switch (weaponFired)
            {
                case Weapon.WeaponType.Rifle:
                    weaponSource.PlayOneShot(rifleGunfire);
                    break;
            }

            CmdPlayGunfireSound(weaponFired);
        }
    }

    [Command]
    void CmdPlayGunfireSound(Weapon.WeaponType weaponFired)
    {
        RpcPlayGunfireSound(weaponFired);
    }

    /// <summary>
    /// Play this sound for the local player only.
    /// </summary>
    /// <param name="weaponFired"></param>
    public void PlayNoAmmoSound(Weapon.WeaponType weaponFired)
    {
        switch (weaponFired)
        {
            case Weapon.WeaponType.Rifle:
                weaponSource.PlayOneShot(rifleNoAmmo);
                break;
        }
    }

    public void PlayReloadStartSound(Weapon.WeaponType weaponFired)
    {
        switch (weaponFired)
        {
            case Weapon.WeaponType.Rifle:
                weaponSource.PlayOneShot(rifleReloadStart);
                break;
        }
    }

    public void PlayReloadEndSound(Weapon.WeaponType weaponFired)
    {
        switch (weaponFired)
        {
            case Weapon.WeaponType.Rifle:
                weaponSource.PlayOneShot(rifleReloadEnd);
                break;
        }
    }

    #endregion
}
