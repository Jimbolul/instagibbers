﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;

public class Portal : NetworkBehaviour
{

    // This is the gameobject that contains the camera, exit point and portal object.
    [SerializeField]
    private GameObject targetPortalObject;

    // This is the location a player can be sent to when they enter the portal linked to this one.
    [SerializeField]
    private Transform myExitPoint;

    // This is the camera whose view will be displayed on the linked portal.
    [SerializeField]
    private Camera myCamera;

    // The Renderer that contains the RenderTexture.
    [SerializeField]
    private MeshRenderer myRenderer;

    // Set this to true if you want to display what the other portal sees in front of it on this portal's portal object.
    // WARNING: Setting this to 'true' causes massive FPS drop.
    [SerializeField]
    private bool useCamera = false;

    // Just in case I ever need this.
    private Portal targetPortal;

    // This is the location the player entering the portal will be sent to.
    private Transform targetExitPoint;

    void Start()
    {
        if (targetPortalObject == null)
        {
            Debug.Log("No target portal assigned. Skipping portal setup.");
            return;
        }

        if (targetPortalObject.tag == "Portal")
        {
            targetPortal = targetPortalObject.GetComponent<Portal>();
            targetExitPoint = targetPortal.GetExitPoint();

            // Only use the rendered texture when allowed.
            if (useCamera) targetPortal.SetPortalView((RenderTexture)myRenderer.material.mainTexture);
            else myCamera.gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            Teleport(col.gameObject);
        }
    }

    public Transform GetExitPoint()
    {
        return myExitPoint;
    }

    public void SetPortalView(RenderTexture renderTexture)
    {
        myCamera.targetTexture = renderTexture;
    }

    /// <summary>
    /// Relocate the player to the linked portal.
    /// </summary>
    private void Teleport(GameObject player)
    {
        // TODO: Create custom FPS Controller to be able to actually rotate the player from another script.
        player.transform.rotation = targetExitPoint.parent.rotation;

        player.transform.position = targetExitPoint.position;
    }


}
