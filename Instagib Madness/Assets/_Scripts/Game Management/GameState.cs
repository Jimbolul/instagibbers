﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class GameState : NetworkBehaviour
{
    #region Fields
    [SerializeField]
    int maxPlayers = 5;

    [SyncVar]
    public bool gameStarted = false;

    [SyncVar]
    public bool waitingForPlayers = true;

    // This list contains all player ID's
    private SyncListString playersInGame = new SyncListString();

    // The index of the played in the playersInGame List is the same as the index in these lists.
    private SyncListInt killsByPlayerIndex = new SyncListInt();
    private SyncListInt deathsByPlayerIndex = new SyncListInt();

    #endregion

    public override void OnStartServer()
    {
        base.OnStartServer();

        InitializeKillDeathArrays();
    }

    public void AddPlayerToGame(string playerID)
    {
        if (isServer)
        {
            this.playersInGame.Add(playerID);
        }

    }

    public void SetPlayersInGame(List<string> players)
    {
        if (isServer)
        {
            foreach (string id in players)
            {
                playersInGame.Add(id);
            }
        }

    }

    #region Leaderboard stuff

    /// <summary>
    /// TODO: Somehow return the playerID and kills / deaths linked to the leaderboard rank.
    /// </summary>
    /// <param name="playerLeaderboardRank"></param>
    public List<LeaderboardData> GetLeaderboardData()
    {
        if (isClient)
        {
            return CreateLeaderboardData();
        }

        return null;
    }

    public void AwardKill(string playerID)
    {
        if (isServer)
        {
            int index = playersInGame.IndexOf(playerID);
            killsByPlayerIndex[index]++;
        }
    }

    public void AwardDeath(string playerID)
    {
        if (isServer)
        {
            int index = playersInGame.IndexOf(playerID);
            deathsByPlayerIndex[index]++;
        }
    }

    /// <summary>
    /// Pre-generate '0 / 0' scores for the maximum amount of players.
    /// </summary>
    private void InitializeKillDeathArrays()
    {
        if (isServer)
        {
            // Kills
            for (int i = 0; i < maxPlayers; i++)
            {
                killsByPlayerIndex.Add(0);
            }

            // Deaths
            for (int i = 0; i < maxPlayers; i++)
            {
                deathsByPlayerIndex.Add(0);
            }
        }        
    }

    /// <summary>
    /// Create a LeaderboardData for every player in the game and return them as a list.
    /// </summary>
    /// <returns></returns>
    private List<LeaderboardData> CreateLeaderboardData()
    {
        List<LeaderboardData> data = new List<LeaderboardData>();

        foreach (string playerID in playersInGame)
        {
            // Get the kill / death values for this player
            int kills = killsByPlayerIndex[playersInGame.IndexOf(playerID)];
            int deaths = deathsByPlayerIndex[playersInGame.IndexOf(playerID)];

            // Create a new LeaderboardData and add it to the list.
            LeaderboardData newData = new LeaderboardData(playerID, kills, deaths);
            data.Add(newData);
        }

        return data;
    }

    #endregion
}
