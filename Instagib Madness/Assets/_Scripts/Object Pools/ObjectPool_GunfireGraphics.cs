﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// The object pool should be created locally, because sending lots of graphics over the network isn't very efficient.
/// </summary>
public class ObjectPool_GunfireGraphics : MonoBehaviour {

    [SerializeField]
    private Transform gunfireParent;
    [SerializeField]
    private GameObject rifleGunfirePrefab;
    [SerializeField]
    private int rifleGunfireAmount = 10;

    private List<GunfireGraphics_Rifle> ggRifleList;

	// Use this for initialization
	void Start () {
        InitRifle();
	}

    /// <summary>
    /// Create the ggRifleList and fill it with graphics scripts.
    /// </summary>
    private void InitRifle()
    {
        ggRifleList = new List<GunfireGraphics_Rifle>();

        for (int i = 0; i < rifleGunfireAmount; i++)
        {            
            ggRifleList.Add(InstantiateGraphics(rifleGunfirePrefab));
        }
    }

    private GunfireGraphics_Rifle InstantiateGraphics(GameObject prefab)
    {
        GameObject go;
        GunfireGraphics_Rifle ggr;

        // Instantiate the gameobject far away from the play-area
        go = (GameObject)Instantiate(prefab, new Vector3(1000, 1000, 1000), gunfireParent.rotation);

        // Set the parent of this new gameobject for maintainability purposes.
        go.transform.SetParent(gunfireParent);

        // Get the graphics script from the GameObject.
        try
        {
            ggr = go.GetComponent<GunfireGraphics_Rifle>();
            return ggr;
        }
        catch
        {
            Debug.Log("Could not instantiate GunfireGraphics_Rifle. Returning null.");
            return null;
        }        
    }

    /// <summary>
    /// This method is called by Player_GraphicsManager;
    /// </summary>
    /// <param name="origin"></param>
    public void DisplayRifleFireAtLocation(Transform origin, Vector3 targetLocation)
    {
        // This will determine the length of the laser ray.
        float distance = Vector3.Distance(origin.position, targetLocation);

        // Get a random ggr from the pool.
        GunfireGraphics_Rifle ggr = ggRifleList[Random.Range(0, ggRifleList.Count)];

        // Set its length.
        ggr.SetLineRendererLength(distance);

        // Display it.
        ggr.DisplayAtLocation(origin);
    }
	
}
