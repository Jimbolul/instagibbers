﻿using UnityEngine;
using System.Collections;

public class GunfireGraphics_Rifle : MonoBehaviour {

    [SerializeField]
    private LineRenderer lineRenderer;

    [SerializeField]
    private float displayTime = .25f;

    void Start()
    {
        lineRenderer.enabled = false;
    }

    /// <summary>
    /// Move this object to origin and display the graphics.
    /// </summary>
    /// <param name="origin"></param>
    public void DisplayAtLocation(Transform origin)
    {
        transform.position = origin.position;
        transform.rotation = origin.rotation;
        StartCoroutine(DisplayGraphics());
    }

    /// <summary>
    /// Enable the LineRenderer for displayTime seconds to simulate a laser effect.
    /// </summary>
    /// <returns></returns>
    private IEnumerator DisplayGraphics()
    {
        lineRenderer.enabled = true;
        yield return new WaitForSeconds(displayTime);
        lineRenderer.enabled = false;
    }

    public void SetLineRendererLength(float length)
    {
        lineRenderer.SetPosition(1, new Vector3(0, 0, length));
    }
}
